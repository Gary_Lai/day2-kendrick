# ORID
## O:
- Established the important tasking skill, which focuses on how to analyze and split requirement to be reasonable exclusive tasks.
- Learned the usage of context map with an "cute number" example to help out with our tasking and make it distinct. 
- Practiced naming in programing, figured out the naming principle and rules.
- Reviewed on git, mainly frequently used commands and standard commit message like feat, fix, refactor in actual project.
- By practicing multiplication table example, we went through the usage of context map and git again, along with the standard commit messages. 
## R:

- Well benefited and fulfilled
## I:

- The practice of multiplication table and pos machine example is pretty detailed and clear that helps me master usage of tasking process and context map.

## D:

- I'm determined to make good use of tasking skill to split out requirement and use context map more frequently to ravel out specific cases right before coding.